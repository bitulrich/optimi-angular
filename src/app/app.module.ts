import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListadoComponent } from './controllers/listado/listado.component';
import { ListadobisComponent } from './controllers/listadobis/listadobis.component';
import { DetalleComponent } from './controllers/detalle/detalle.component'

@NgModule({
  declarations: [
    AppComponent,
    ListadoComponent,
    ListadobisComponent,
    DetalleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
