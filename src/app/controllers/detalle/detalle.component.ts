import { Component, Input, OnInit } from '@angular/core';

import { Pelicula } from './../../entidades/pelicula'

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  @Input()
  pelicula:Pelicula;

  @Input()
  cadena:string;

  constructor() { }

  ngOnInit(): void {
    
  }

  actualizar():void{
    console.log(this.pelicula)
  }

}
