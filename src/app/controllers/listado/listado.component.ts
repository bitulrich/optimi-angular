import { Component } from '@angular/core';


@Component({
    selector: 'app-listado',
    templateUrl: 'listado.component.html'
})
export class ListadoComponent{
    titulo:string = "Hola desde el template del componente como variable";
    parrafo:string="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea vitae, libero id tempora expedita suscipit dolorum rem cumque, alias aut illo in omnis. Quidem deleniti magnam cumque ex, labore ducimus!";
}
