import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadobisComponent } from './listadobis.component';

describe('ListadobisComponent', () => {
  let component: ListadobisComponent;
  let fixture: ComponentFixture<ListadobisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadobisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadobisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
