import { Component, OnInit } from '@angular/core';
import { Pelicula } from './../../entidades/pelicula'

const PELICULAS:Pelicula[] = [
  new Pelicula(1,'Los vengadores','accion','./../../../assets/img/ferdinand.jpg'),
  new Pelicula(2,'La llamada','Terror','./../../../assets/img/malditos.jpg'),
  new Pelicula(3,'Shrek','infantil','./../../../assets/img/pendular.jpg'),
]

@Component({
  selector: 'app-listadobis',
  templateUrl: './listadobis.component.html',
  styleUrls: ['./listadobis.component.css']
})
export class ListadobisComponent implements OnInit {
  titulo:string="Listado de películas"
  peliculas:Pelicula[]=PELICULAS;
  peliculaSeleccionada:Pelicula;
  cadena:string="Esta es una cadena";
  constructor() { }

  ngOnInit(): void {
    console.log(this.peliculas)
  }

  editarFila(pelicula:Pelicula):void{
    this.peliculaSeleccionada = pelicula;
  }

}
